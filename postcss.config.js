module.exports = {
  plugins: [
    [
      "postcss-jit-props",
      { files: ["node_modules/open-props/open-props.min.css"] },
    ],
    "postcss-flexbugs-fixes",
    [
      "postcss-preset-env",
      {
        autoprefixer: {
          flexbox: "no-2009",
        },
        stage: 3,
        features: {
          "custom-properties": false,
        },
      },
    ],
  ],
};
